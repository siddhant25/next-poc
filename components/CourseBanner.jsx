export default function CourseBanner(props) {
  // console.log(props);
  // return;
  const { post, user } = props;
  const { title, body } = post;
  const { name } = user;

  // console.log(title, body, name);
  // return <h1>Hello</h1>;
  return (
    <div>
      <h1>{title}</h1>
      <div>
        <p>{body}</p>
        <p>{name}</p>
      </div>
    </div>
  );
}
