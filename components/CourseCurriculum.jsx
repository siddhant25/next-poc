import { useEffect, useState } from "react";

export default function CourseCurriculum() {
  const [curriculum, setCurriculum] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setCurriculum(json);
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  return (
    <div>
      {curriculum.map((section) => {
        return (
          <div key={section.id}>
            <h4>{section.title}</h4>
            <p>{section.body}</p>
          </div>
        );
      })}
    </div>
  );
}
