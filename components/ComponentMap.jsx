import identifierMapper from "../IdentifierMapper";
import CourseDetails from "./CourseDetails";
import Reviews from "./Reviews";
import InnerHTML from 'dangerously-set-html-content'

export default function ComponentMap(props) {
  const block = props;
  const section_key = block.section_type_identifier;
  if (block.section_type_identifier in identifierMapper) {
    // return <div>Dynamic</div>;
    return identifierMapper[section_key](props.requiredProps);
  } else {
    // return <div> Static </div>;
    if(props.section_type == "generic")
    {
        return <div dangerouslySetInnerHTML={{ __html: block.html }} />;
    }
    else{
        return <InnerHTML html={props.html} />;
    }
  }
}
