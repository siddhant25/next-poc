// import path from "path";
import axios from "axios";
import { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import ReactDOMServer from "react-dom/server";
// import ejs from "ejs";
import TrialComponent from "../components/ComponentMap";
// import InnerHTML from 'dangerously-set-html-content'

// POC #1
// export async function getServerSideProps() {
//   const response = await axios({
//     method: "GET",
//     url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/StaticTry.ejs",
//   });
//   console.log(response.data);
//  return {
//     props: {
//       page: { __html: response.data },
//     },
//   };
// }

// POC #2
// export async function getServerSideProps() {
//   const response = await axios({
//     method: "GET",
//     url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/trial.html",
//   });
//   // const template_dependencies = {
//   //   Heading: "First Heading Dynamic",
//   //   Subheading: "2nd Heading Dynamic",
//   //   heading3: "3rd Heading Dynamic",
//   //   heading4: "4th Heading Dynamic",
//   // };
//   var Component = response.data;
//   return {
//     props: {
//       //   page: {__html: "<div class=\"app-container\"><div class=\"toolbar\"><div class=\"notification\">Notification</div></div><div class=\"canvas-container\"><div class=\"init-loading\"><i class=\"icon-spin1 animate-spin\"></i></div><canvas id=\"graph\" class=\"canvas\" width=\"3840\" height=\"2400\"></canvas></div></div>"},
//       Component: Component,
//       PageProps: {
//         page: '',
//       },
//     },
//   };
// }

// POC #3
export async function getServerSideProps() {
  const response = await axios({
    method: "GET",
    url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/try.ejs",
  });
  const template_dependencies ={
    Heading: "First Heading Dynamic",
    Subheading: "2nd Heading Dynamic",
    heading3: "3rd Heading Dynamic",
    heading4: "4th Heading Dynamic",
  }
  let template = ejs.render(response.data, template_dependencies);
 return {
    props: {
      //   page: {__html: "<div class=\"app-container\"><div class=\"toolbar\"><div class=\"notification\">Notification</div></div><div class=\"canvas-container\"><div class=\"init-loading\"><i class=\"icon-spin1 animate-spin\"></i></div><canvas id=\"graph\" class=\"canvas\" width=\"3840\" height=\"2400\"></canvas></div></div>"},
      page: { __html: template },
    },
  };
}

//POC #4
// export async function getServerSideProps() {
//   return {
//     props:{
//       str: "hiiiiiiiiiiiiiii"
//     }
//   }
// }

export default function Home(props) {
  // const [state, setState] = useState({ Component: "" });
  // ------------POC #1 and #2
  // const { page } = props;
  // return <div dangerouslySetInnerHTML={page} />;

  // var PageComp = <div> Hellloooooo!!!!!!</div>
  // // useEffect(() => {
  // //   // var div = document.createElement('div');
  // //   // console.log(div);
  // //   PageComp = ReactDOM.render(<Component />, div);
  // // }, [])

  // // POC #3
  // const { Component, PageProps } = props;
  // console.log(Component);


  // console.log(props);
  // console.log(Component);
  // // console.log(PageComp);
  // // return PageComp
  // return (<Component page={props.PageProps} />);
  // console.log(TrialComp);
  // return <Component PageProps />;

  
  // return <div dangerouslySetInnerHTML={{__html: Component}} />;
  // return  <InnerHTML html={Component} />;

  //POC #4
  // const {str} = props;

  // return <div>{str}</div>;

}
