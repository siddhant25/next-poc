import axios from "axios";
import fs from "fs/promises";
import path from "path";
import { useEffect, useState } from "react";
import InnerHTML from "dangerously-set-html-content";
import Script from "next/script";

const page = {
  page_url:
    "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/newtry.json",
};

async function getPaths() {
  const data = await fs.readFile(path.join(process.cwd(), "valid-paths.json"));
  const paths = JSON.parse(data).staticPaths;
  return paths;
}

export async function getStaticPaths() {
  const paths = await getPaths();
  const pathsToReturn = await paths.map((path) => {
    return { params: path };
  });
  // console.log(pathsToReturn);

  return {
    // paths: [
    //   { params: { host: "localhost" } },
    //   { params: { host: "test.localhost" } },
    // ],
    // paths: [],
    paths: pathsToReturn,
    fallback: "blocking",
  };
}

export async function getStaticProps(context) {
  // console.log('runnnnnninnnnnngggggg!!!!!!!!!!!');
  const filePath = path.join(process.cwd(), "data", "dummy-backend.json");
  const jsonData = await fs.readFile(filePath);
  const data = JSON.parse(jsonData);

  const paths = await getPaths();
  const verifiedPath = paths.find((path) => path.host == context.params.host);

  if (!verifiedPath) {
    return { notFound: true };
  }

  console.log(context.params);
  return { props: { products: data.products, host: context.params.host } };
}

export default function Home(props) {
  const [word, setWord] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios("http://localhost:8080/render", {
      method: "POST",
      data: page,
    })
      .then((response) => response.data)
      .then((json) => {
        console.log('here', json);
        setWord(json.page);
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  const { products, host } = props;
  if (isLoading) {
    return <div>Loading...</div>;
  }
  return (
    <div>
      <h1>This is {host} page!</h1>
      <ul>
        {products.map((p) => (
          <li key={p.id}>{p.title}</li>
        ))}
      </ul>
      <p>{word}</p>
      <InnerHTML html={word} />;
    </div>
  );
}
