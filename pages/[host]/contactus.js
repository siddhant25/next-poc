import fs from "fs";
import path from "path";

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking",
  };
}

export async function getStaticProps(context) {
  const data = fs
    .readFileSync(path.join(process.cwd(), "valid-paths.json"))
    .toString();
  const paths = JSON.parse(data).staticPaths;
  const toReturn = paths.find((path) => path.host == context.params.host);
  console.log(toReturn);
  return {
    props: toReturn,
  };
}

export default function contactUs(props) {
  console.log(props);
  return <div> Contact {props.host} Page!</div>;
}
