// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from "axios";

const populatePosts = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: "https://jsonplaceholder.typicode.com/posts/1",
    })
      .then((response) => response.data)
      .then((data) => {
        resolve(data);
      })
      .catch((err) => reject(err));
  });
};

const populateUser = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: "https://jsonplaceholder.typicode.com/users/1",
    })
      .then((response) => response.data)
      .then((data) => {
        resolve(data);
      })
      .catch((err) => reject(err));
  });
};

const populateAllPosts = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: "https://jsonplaceholder.typicode.com/posts",
    })
      .then((response) => response.data)
      .then((data) => {
        resolve(data);
      })
      .catch((err) => reject(err));
  });
}


//----------------- EJS - All Server Side
// export default function handler(req, res) {
//   let Result = {
//     post: null,
//     user: null,
//     allPosts: []
//   }
//   return Promise.all([populatePosts(), populateUser(), populateAllPosts()])
//     .then((Data) => {
//       Result.post = Data[0];
//       Result.user = Data[1];
//       Result.allPosts = Data[2];
//       res.status(200).json(Result);
//     })
//     .catch((err) => console.log(err));
// }

//----------------- Next Components Method all posts client side
// export default function handler(req, res) {
//   let Result = {
//     post: null,
//     user: null
//   }
//   // res.json('hi');
//   return Promise.all([populatePosts(), populateUser()])
//     .then((Data) => {
//       Result.post = Data[0];
//       Result.user = Data[1];
//       res.status(200).json(Result);
//     })
//     .catch((err) => console.log(err));
// }

export default function handler(req, res) {
  let Result = {
    post: null,
    user: null
  }
  return Promise.all([populatePosts(), populateUser()])
    .then((Data) => {
      Result.post = Data[0];
      Result.user = Data[1];
      Result.allPosts = Data[2];
      res.status(200).json(Result);
    })
    .catch((err) => console.log(err));
}