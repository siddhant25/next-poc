import axios from "axios";
import ComponentMap from "../components/ComponentMap";
import identifierAPIMapper from "../identifierAPIMapper";
import ejs from "ejs";
import InnerHTML from "dangerously-set-html-content";
import fs from "fs";
import Script from "next/script";

const fetcher = (url) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: url,
    })
      .then((response) => {
        let jsonData = response.data;
        return jsonData;
      })
      .then((data) => resolve(data))
      .catch((err) => reject(err));
  });
};

async function AsyncMap(arr, toObj) {
  const current_domain = "http://localhost:3000";
  const toResolve = arr.map(async (item) => {
    if (item.section_type_identifier in identifierAPIMapper) {
      let url =
        current_domain + identifierAPIMapper[item.section_type_identifier];
      const response = await axios({ method: "GET", url: url });
      toObj[item.section_type_identifier] = response.data;
      return;
    } else {
      toObj[item.section_type_identifier] = null;
      return;
    }
  });
  const resolved = await Promise.all(toResolve);
  return resolved;
}

// export function getServerSideProps() {
//   const current_domain = "http://localhost:3000";
//   return axios({
//     method: "GET",
//     url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/test.json",
//   })
//     .then((response) => {
//       let jsonData = response.data;
//       return jsonData;
//     })
//     .then((data) => {
//       let specificProps = {};
//       return AsyncMap(data.blocks, specificProps)
//         .then((prommiseArray) => {
//           return {
//             props: {
//               blocks: data.blocks,
//               specificProps: specificProps,
//             },
//           };
//         })
//         .then((returnData) => {
//           // console.log(dataset);
//           return returnData;
//         })
//         .catch((err) => console.log(err));

//       // data.blocks.map((item) => {
//       //   if (item.section_type_identifier in identifierAPIMapper) {
//       //     let url =
//       //       current_domain + identifierAPIMapper[item.section_type_identifier];
//       //     console.log("url", url);
//       //     fetcher(url)
//       //       .then((propData) => {
//       //         console.log("propData", propData);
//       //         specificProps.push(propData);
//       //       })
//       //       .catch((err) => console.log(err));
//       //   } else {
//       //     specificProps.push(null);
//       //   }
//       // });
//     })
//     .catch((err) => console.log(err));

//   // const fileData = JSON.parse(
//   //   fs.readFileSync("/home/siddhant/Desktop/test.json")
//   // );
//   // return {
//   //   props: {
//   //     blocks: fileData.blocks,
//   //   },
//   // };
// }

// EJS try
// export function getServerSideProps() {
//   const current_domain = "http://localhost:3000";
//   return axios({
//     method: "GET",
//     url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/newtry.ejs",
//   })
//     .then((response) => response.data)
//     .then((template) => {
//       let template_dependencies = {
//         banner_title: "",
//         banner_body: "",
//         banner_author: "",
//         posts: [],
//       }
//       console.log(template);

//       return fetcher(current_domain + "/api/banner")
//       .then((data) => {
//         template_dependencies.banner_title = data.post.title;
//         template_dependencies.banner_body = data.post.body;
//         template_dependencies.banner_author = data.user.name;
//         template_dependencies.posts = data.allPosts;
//         console.log(template_dependencies);
//         return [template_dependencies, template];
//       })
//       .then(template_data => {
//         let page = ejs.render(template_data[1], template_data[0]);
//         return {
//           props: {
//             page: page
//           }
//         }
//       })
//     });
// }

//EJS try 2 with client side EJS - fs implementation
export function getServerSideProps() {
  const current_domain = "http://localhost:3000";
  let template_dependencies = {
    banner_title: "",
    banner_body: "",
    banner_author: "",
  };
  let template = fs
    .readFileSync("/home/siddhant/Desktop/newtry.ejs")
    .toString();
  console.log(template);
  return fetcher(current_domain + "/api/banner")
    .then((data) => {
      template_dependencies.banner_title = data.post.title;
      template_dependencies.banner_body = data.post.body;
      template_dependencies.banner_author = data.user.name;
      template_dependencies.posts = [];
      return [template_dependencies, template];
    })
    .then((template_data) => {
      let page = ejs.render(template_data[1], template_data[0]);
      return {
        props: {
          page: page,
        },
      };
    });
}

//EJS try 2 with client side EJS - S3 implementation
// export function getServerSideProps() {
//   const current_domain = "http://localhost:3000";
//   return axios({
//     method: "GET",
//     url: "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/newtry.ejs",
//   })
//     .then((response) => response.data)
//     .then((template) => {
//       let template_dependencies = {
//         banner_title: "",
//         banner_body: "",
//         banner_author: "",
//       };
//       console.log(template);

//       return fetcher(current_domain + "/api/banner")
//         .then((data) => {
//           template_dependencies.banner_title = data.post.title;
//           template_dependencies.banner_body = data.post.body;
//           template_dependencies.banner_author = data.user.name;
//           template_dependencies.posts = [];
//           console.log(template_dependencies);
//           return [template_dependencies, template];
//         })
//         .then((template_data) => {
//           let page = ejs.render(template_data[1], template_data[0]);
//           return {
//             props: {
//               page: page,
//             },
//           };
//         });
//     });
// }

export default function Page(props) {
  // console.log("HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEE", props);
  // const { blocks, specificProps } = props;
  // return (
  //   <div>
  //     {blocks.map((v, i) => {
  //       // console.log(v);
  //       return (
  //         <div>
  //           <ComponentMap
  //             key={i}
  //             name={v.name}
  //             section_type={v.section_type}
  //             section_type_identifier={v.section_type_identifier}
  //             html={v.html}
  //             requiredProps={specificProps[v.section_type_identifier]}
  //           />
  //           <hr />
  //         </div>
  //       );
  //     })}
  //   </div>
  // );

  return (
    <>
      <Script
        src="https://unpkg.com/ejs@3.1.6/ejs.min.js"
        strategy="beforeInteractive"
      />
      <Script id="show-posts" strategy="afterInteractive">
        {`console.log(ejs);
        var section_str =
            '<% posts.forEach(post=> { %>' +
            '<div>' +
            '<h4>' +
            '<%= post.title %>' +
            '</h4>' +
            '<p>'
            +
            '<%= post.body %>'
            +
            '</p>'
            +
            '</div >' +
            '<% }) %>';
        fetch('https://jsonplaceholder.typicode.com/posts').then(function (response) {
            // The API call was successful!
            return response.json();
        })
            .then(function (data) {
                console.log('success!', data);
    
                console.log(section_str);
                let fn = ejs.compile(section_str, { client: true });
                let html = fn({ posts: data });
                console.log(html);
                document.getElementById('client-side').innerHTML = html;
            })
            .catch(function (err) {
                // There was an error
                console.warn('Something went wrong.', err);
            });
        `}
      </Script>
      {/* {console.log(ejs)} */}
      <InnerHTML html={props.page} />;
    </>
  );
  // return <div>This is a test!</div>;
}
