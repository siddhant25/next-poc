var Template = `<div class="is-section is-section-100 is-box is-content-top">
    <div class="is-overlay" style="background:var(--eds-light-gradient)"></div>
    <div class="is-boxes">
        <div class="is-box-centered is-content-top edge-y-1">
            <div class="is-container container-fluid-fluid is-content-1200" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-12" style="text-align: left;">
                        <h1 style="color: var(--eds-dark); text-align: center; padding-top:30px;"
                            class="eds-section-heading">Course Instructor<br></h1>
                        <p class="eds-section-subheading"
                            style="color: var(--eds-dimdark); text-align: center; margin: 20px auto; max-width: 700px;">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren.<br></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <section class="eds-teacher-section">
                    <% tutors.forEach(tutor => { %> 
                    <div class="eds-teacher" data-noedit=""> <img src="./assets/designs/images/edmingle/person-1.jpg"
                            alt="categories image" class="eds-teacher-image">
                        <div>
                            <h3 class="eds-teacher-name"><%= tutor.tutor_name %></h3>
                            <p class="eds-teacher-designation">designation of Instructor</p>
                            <p class="eds-teacher-description"><%= tutor.about%></p>
                        </div>
                    </div>
                    <% }); %> 
                </section>
            </div>
        </div>
    </div>
</div>`

let serviceUrl = "http://localhost/nuSource/api/v1/organization/bundles/204?get_tutors=1"
fetch(serviceUrl).then(response => response.json()).then(data => {
    let tutors = data.tutors;
    console.log(tutors);
    return ejs.render(Template, { tutors: tutors });
}).then((renderedTemplate) => {
    let sections = document.getElementsByName('course_instructor');
    sections.forEach((section) => {
        section.innerHTML = renderedTemplate;
    });
});