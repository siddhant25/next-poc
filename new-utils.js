var section_str =
  "<% posts.forEach(post=> { %>" +
  "<div>" +
  "<h4>" +
  "<%= post.title %>" +
  "</h4>" +
  "<p>" +
  "<%= post.body %>" +
  "</p>" +
  "</div >" +
  "<% }) %>";
console.log(section_str);

fetch("https://jsonplaceholder.typicode.com/posts")
  .then(function (response) {
    // The API call was successful!
    return response.json();
  })
  .then(function (data) {
    console.log("success!", data);

    console.log(section_str);
    let fn = ejs.compile(section_str, { client: true });
    let html = fn({ posts: data });
    console.log(html);
    document.getElementById("client-side").innerHTML = html;
  })
  .catch(function (err) {
    // There was an error
    console.warn("Something went wrong.", err);
  });
