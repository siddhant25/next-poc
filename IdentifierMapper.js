import CourseBanner from "./components/CourseBanner";
import CourseDetails from "./components/CourseDetails";
import CourseCurriculum from "./components/CourseCurriculum";
import Reviews from "./components/Reviews";

const identifierMapper = {
  reviews: function (props) {
    console.log("hereeeeeeeee", props);
    return <Reviews {...props} />;
  },
  course_details: function (props = null) {
    return <CourseDetails {...props} />;
  },
  course_banner: function (props = null) {
    return <CourseBanner {...props} />;
  },
  course_curriculum: function (props = null){
    return <CourseCurriculum />
  }
};

export default identifierMapper;
